from flask import Flask, request, make_response, render_template
from flask_restful import Resource, Api

app = Flask(__name__)
api = Api(app)

class Home(Resource):
    def __init__(self):
        pass

    def get(self):
        headers = {'Content-Type': 'text/html'}
        return make_response(render_template('index.html'),200,
                                              headers)

class Calculator(Resource):
    message = "success"

    def validate(self):
        self.message = "success"
        return True

    def parse(self, request):
        args = request.args
        try:
            self.a = int(args['a'])
            self.b = int(args['b'])
        except KeyError:
            self.message = "either or both arguments are missing"
            return False
        except ValueError:
            self.message = "either or both arguments are not integer"
            return False
        return True

    def get(self):
        if self.parse(request) and self.validate():
            return {"result": self.get_result()}, 200
        else:
            return {"error": self.message}, 500


    def get_result(self):
        return None

class Adder(Calculator):
    def get_result(self):
        return self.a + self.b

class Subtractor(Calculator):
    def get_result(self):
        return self.a - self.b

class Multiplier(Calculator):
    def get_result(self):
        return self.a * self.b

class Divider(Calculator):
    def validate(self):
        if self.b == 0:
            self.message = "divisor is zero"
            return False
        return True

    def get_result(self):
        return self.a / self.b

api.add_resource(Home, '/')
api.add_resource(Adder, '/add')
api.add_resource(Subtractor, '/sub')
api.add_resource(Multiplier, '/mul')
api.add_resource(Divider, '/div')

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
