import unittest
from main import app

class CalculatorApiTestCase(unittest.TestCase):
    def setUp(self):
        self.app = app
        self.client = self.app.test_client()

    def test_add(self):
        response = self.client.get('/add?a=2&b=2')
        expected_resp = {'result': 4}
        self.assertEqual(response.status_code, 200)
        self.assertDictEqual(response.get_json(), expected_resp)

    def test_when_div_by_zero_error_is_returned(self):
        response = self.client.get('/div?a=2&b=0')
        expected_resp = {'error': 'divisor is zero'}
        self.assertEqual(response.status_code, 500)
        self.assertDictEqual(response.get_json(), expected_resp)

    def test_when_argument_is_not_int_error_is_returned(self):
        response = self.client.get('/div?a=2&b=sss')
        expected_resp = {'error': 'either or both arguments are not integer'}
        self.assertEqual(response.status_code, 500)
        self.assertDictEqual(response.get_json(), expected_resp)

    def test_when_argument_is_missing_error_is_returned(self):
        response = self.client.get('/div?a=2')
        expected_resp = {'error': 'either or both arguments are missing'}
        self.assertEqual(response.status_code, 500)
        self.assertDictEqual(response.get_json(), expected_resp)

if __name__ == '__main__':
    unittest.main()
